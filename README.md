# Project EDAF75

**Report by:**  
Adina Borg, ad4600bo-s    
Tove Sölve, to5185so-s 

### UML diagram


![alt text](https://gitlab.com/tovesolve1/edaa75-krusty-kookies/raw/master/Diagram1.png)










### Relations

ingredients(**name**, quantity, lastdelivery, lastdeliveryamount, unit)  
recipes(amount, **_name_**, **_name_**)  
cookies(**name**)  
pallets(**pallid**, productiondate, blocked, location, _name_, _orderid_)  
orders(**orderid**, orderdate, delivered, delivereddate, _name_)  
customers(**name**, address)  
deliveries(nbrDelivered, **_name_**,**_orderid_**)  


### Link to SQL

[link to SQL](create-schema.sql)  



### How we run our server

step1: sqlite3 krusty.sqlite < create-schema.sql     
step2: ./gradlew run