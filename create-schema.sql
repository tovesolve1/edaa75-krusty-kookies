-- SQL script to create the tables necessary for the project in EDAF75.
-- MySQL version.
--
--
-- We disable foreign key checks temporarily so we can delete the
-- tables in arbitrary order, and so insertion is faster.

PRAGMA foreign_keys=off;

-- Drop the tables if they already exist.

DROP TABLE IF EXISTS ingredients;
CREATE TABLE ingredients(
  name           TEXT NOT NULL,
  quantity             INT,
  last_delivery        DATE,
  last_delivery_amount INT,
  unit                 TEXT NOT NULL,
  PRIMARY KEY (name)
);

DROP TABLE IF EXISTS recipes;
CREATE TABLE recipes (
   i_name    TEXT NOT NULL,
   name        TEXT NOT NULL,
   amount        INT NOT NULL,
   
  PRIMARY KEY   (i_name, name),
  FOREIGN KEY   (i_name) REFERENCES ingredients(name),
  FOREIGN KEY   (name) REFERENCES cookies(name)
);

DROP TABLE IF EXISTS cookies;
CREATE TABLE cookies (
  name        TEXT NOT NULL,
  PRIMARY KEY   (name)
);

DROP TABLE IF EXISTS pallets;
CREATE TABLE pallets (
  pall_id         TEXT NOT NULL DEFAULT (lower(hex(randomblob(16)))),
  production_date DATE DEFAULT CURRENT_DATE,
  blocked         BOOLEAN DEFAULT FALSE,
  location        TEXT NOT NULL DEFAULT 'Freezestorage', /*De locations som finns är freestorage, ramp, deliveredtostore*/
  cookie          TEXT NOT NULL,
  order_id        INTEGER NOT NULL DEFAULT 0,
  
  PRIMARY KEY   (pall_id),
  FOREIGN KEY (cookie) REFERENCES cookies(name),
  FOREIGN KEY (order_id) REFERENCES orders(order_id)
);

DROP TABLE IF EXISTS orders;
CREATE TABLE orders (
  order_id       INTEGER NOT NULL,
  order_date     DATE,
  delivered      INT NOT NULL,
  delivered_date DATE,
  name           TEXT NOT NULL,
  
  PRIMARY KEY (order_id),
  FOREIGN KEY (name) REFERENCES customers(name)
);

DROP TABLE IF EXISTS customers;
CREATE TABLE customers (
  name      TEXT NOT NULL,
  address   TEXT NOT NULL,
  
  PRIMARY KEY (name)
);

DROP TABLE IF EXISTS deliveries;
CREATE TABLE deliveries (
  nbrDelivered   INT NOT NULL,
  name         TEXT NOT NULL,
  order_id       INTEGER NOT NULL,
  
  PRIMARY KEY (name, order_id),
  FOREIGN KEY (name) REFERENCES cookies(name),
  FOREIGN KEY (order_id) REFERENCES orders(order_id)
);


PRAGMA foreign_keys = on;
